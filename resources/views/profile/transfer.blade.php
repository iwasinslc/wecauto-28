@extends('layouts.profile')
@section('title', __('Transfer'))
@section('content')



    <section class="section-tabs">
        <div class="container">

            @include('partials.profile.finance_menu')

                <div class="section-tabs__content">
                    <form class="tabs-form" method="POST" action="{{ route('profile.transfer.store') }}">
                        {{ csrf_field() }}
                   

                        
                        <div class="tabs-form__row">
                            <div class="tabs-form__col-6">
                                <div class="field">
                                    <label>{{__('Amount')}}</label>
                                    <div class="field__group">
                                        <label class="select">
                                            <select name="wallet_id">
                                                <option value="{{getUserWallet('WEC')->id}}">WEC</option>
{{--                                                @if (user()->hasRole('root'))--}}
                                                    <option value="{{getUserWallet('FST')->id}}">FST</option>
{{--                                                @endif--}}
                                            </select>
                                        </label>
                                        <input  type="number" class="js-input-number" value="{{old('amount', 0.1)}}" name="amount" required>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs-form__col-6">
                                 {{-- <p class="tabs-form__subtext">Comission = 1%--}}
                                {{--  </p>--}}
                              <p class="tabs-form__subtext">{{__('FST Transfer')}} {{__('Limit')}} {{number_format(user()->sellLimit(), 2)}} USD
                                                    ({{number_format(user()->sellLimit()*rate('USD', 'FST'), 2)}} FST), {{__('Fee for WEC')}} 1%;
                              </p>
                            </div>

                        </div>
                        <div class="tabs-form__row">
                            <div class="tabs-form__col-6">
                                <div class="field">
                                    <label>{{__('Login')}}</label>
                                    <input  type="text" value="{{old('login')}}" name="login" required>
                                </div>
                            </div>
                            <div class="tabs-form__col-6">
                                <div class="field">
                                    <label>{{__('E-mail')}}</label>
                                    <input  type="email" value="{{old('email')}}" name="email" required>
                                </div>
                            </div>
                        </div>


{{--                        <input type="hidden" name="wallet_id" value="{{getUserWallet('WEC')->id}}">--}}
                        <div class="tabs-form__bottom">
                            <button class="btn btn--warning btn--size-md">{{__('Send')}}
                            </button>
                        </div>
                    </form>
                </div>


        </div>
    </section>


    @include('partials.profile.finance_filter')
    <!-- /.card -->
@endsection